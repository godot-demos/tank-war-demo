extends RigidBody2D

@onready var game_main = get_node("/root/GameMain")
@onready var score = get_node("/root/GameMain/Box/Score")

func _ready():
	pass

func _on_Bullet_body_entered(body):
	print("子弹碰撞")
	if game_main.game_over:
		
		return
		
	if body is PhysicsBody2D:
		if body.name != "MyTank":
			body.queue_free()
			game_main.score += 1
			score.text = str(game_main.score)
		else:
			print("游戏结束")
			game_main.game_over = true
	self.queue_free()
