extends Node2D

@export var bullet_scene: PackedScene = preload("res://bullet/Bullet.tscn")
@export var enemy_scene: PackedScene = preload("res://enemy/Enemy.tscn")

var score = 0
var game_over = false

func _ready():
	$MyTank/BulletTimer.connect("timeout", Callable(self, "_on_BulletTimer_timeout"))
	
func _physics_process(delta):
	if game_over:
		$Center/GameOverTip.visible = true
		get_tree().paused = true
		
	if Input.is_key_pressed(KEY_SPACE):
		if $MyTank/BulletTimer.is_stopped():
			$MyTank/BulletTimer.start(0.2)
	else:
		$MyTank/BulletTimer.stop()
	# 上
	if Input.is_key_pressed(KEY_W) or Input.is_key_pressed(KEY_UP):
		$MyTank/AnimatedSprite2D.rotation_degrees = 0
		$MyTank/AnimatedSprite2D.play("default")
		$MyTank.move_and_collide(Vector2.UP)
		$MyTank.direction = Vector2.UP
	# 左
	elif Input.is_key_pressed(KEY_A) or Input.is_key_pressed(KEY_LEFT):
		$MyTank/AnimatedSprite2D.rotation_degrees = -90
		$MyTank/AnimatedSprite2D.play("default")
		$MyTank.move_and_collide(Vector2.LEFT)
		$MyTank.direction = Vector2.LEFT
	# 右
	elif Input.is_key_pressed(KEY_D) or Input.is_key_pressed(KEY_RIGHT):
		$MyTank/AnimatedSprite2D.rotation_degrees = 90
		$MyTank/AnimatedSprite2D.play("default")
		$MyTank.move_and_collide(Vector2.RIGHT)
		$MyTank.direction = Vector2.RIGHT
	# 下
	elif Input.is_key_pressed(KEY_S) or Input.is_key_pressed(KEY_DOWN):
		$MyTank/AnimatedSprite2D.rotation_degrees = 180
		$MyTank/AnimatedSprite2D.play("default")
		$MyTank.move_and_collide(Vector2.DOWN)
		$MyTank.direction = Vector2.DOWN
	else:
		$MyTank/AnimatedSprite2D.stop()


func _on_BulletTimer_timeout():
	fire($MyTank)
	
	
func fire(subject):
	var bullet = bullet_scene.instantiate()
	bullet.position = subject.position + subject.direction * 20
	bullet.linear_velocity = subject.direction * 300
	bullet.gravity_scale = 0
	bullet.contact_monitor = true
	bullet.max_contacts_reported = 1
	self.add_child(bullet)


func _on_EnemyGenTimer_timeout():
	print("计时器运行")
	var enemy = enemy_scene.instantiate()
	enemy.position = Vector2(100, 100)
	self.add_child(enemy)
	
