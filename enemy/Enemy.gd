extends CharacterBody2D

@export var bullet_scene: PackedScene = preload("res://bullet/Bullet.tscn")
@onready var game_main = get_node("/root/GameMain")

var direction = Vector2.ZERO
var arrived = true

func _ready():
	$RunTimer.one_shot = false
	$RunTimer.start(random_time())
	$RayCast3D.enabled = true

func _physics_process(delta):
	
	if not arrived:
		self.move_and_collide(direction)
	else:
		var i = randi() % 4
		match i:
			0:
				direction =  Vector2.UP
				$AnimatedSprite2D.rotation_degrees = 0
			1:
				direction =  Vector2.LEFT
				$AnimatedSprite2D.rotation_degrees = -90
			2:
				direction =  Vector2.RIGHT
				$AnimatedSprite2D.rotation_degrees = 90
			_:
				direction =  Vector2.DOWN
				$AnimatedSprite2D.rotation_degrees = 180
		arrived = false
		
	if direction != Vector2.ZERO:
		$RayCast3D.target_position = direction * 5000
		if $RayCast3D.is_colliding():
			var collider = $RayCast3D.get_collider()
			if collider is PhysicsBody2D and collider.name == "MyTank":
				game_main.fire(self)
				

func _on_RunTimer_timeout():
	arrived = true
		

func random_time():
	return randf_range(1.2, 5.6)
